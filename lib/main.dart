import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String customerName;
  String customerID;
  String customerEmail;

  getcustomerName(name) {
    customerName = name;
  }

  getcustomerID(id) {
    customerID = id;
  }

  getcustomerEmail(email) {
    customerEmail = email;
  }

  createData() {
    DocumentReference documentReference =
        Firestore.instance.collection("MyCustomers").document(customerName);

    // create Map for the customer
    Map<String, dynamic> customers = {
      "customerName": customerName,
      "customerID": customerID,
      "customerEmail": customerEmail
    };

    documentReference.setData(customers).whenComplete(() { });
  }

  readData() {
    DocumentReference documentReference =
        Firestore.instance.collection("MyCustomers").document(customerName);

    documentReference.get().then((datasnapshot) { });
  }

  updateData() {
    DocumentReference documentReference =
        Firestore.instance.collection("MyCustomers").document(customerName);

    // create Map
    Map<String, dynamic> customers = {
      "customerName": customerName,
      "customerID": customerID,
      "customerEmail": customerEmail
    };

    documentReference.setData(customers).whenComplete(() {});
  }

  deleteData() {
    DocumentReference documentReference =
        Firestore.instance.collection("MyCustomers").document(customerName);

    documentReference.delete().whenComplete(() {
      print("$customerName deleted");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("myMTN App 2022 CRUD"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: TextFormField(
                decoration: const InputDecoration(
                    labelText: "Name",
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blue, width: 2.0))),
                onChanged: (String name) {
                  getcustomerName(name);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: TextFormField(
                decoration: const InputDecoration(
                    labelText: "Customer ID",
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blue, width: 2.0))),
                onChanged: (String id) {
                  getcustomerID(id);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: TextFormField(
                decoration: const InputDecoration(
                    labelText: "Customer e-Mail",
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blue, width: 2.0))),
                onChanged: (String customer) {
                  getcustomerEmail(email);
                },
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ElevatedButton(
                  child: const Text("Create"),
                  onPressed: () {
                    createData();
                  },
                ),
                ElevatedButton(
                  child: const Text("Read"),
                  onPressed: () {
                    readData();
                  },
                ),
                ElevatedButton(
                  child: const Text("Update"),
                  onPressed: () {
                    updateData();
                  },
                ),
                ElevatedButton(
                  child: const Text("Delete"),
                  onPressed: () {
                    deleteData();
                  },
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                textDirection: TextDirection.ltr,
                children: const <Widget>[
                  Expanded(
                    child: Text("Name"),
                  ),
                  Expanded(
                    child: Text("Customer ID"),
                  ),
                  Expanded(
                    child: Text("Customer e-Mail"),
                  ),
                ],
              ),
            ),
            StreamBuilder(
              stream: Firestore.instance.collection("MyCustomers").snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) {
                        DocumentSnapshot documentSnapshot =
                            snapshot.data.documents[index];
                        return Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(documentSnapshot["customerName"]),
                            ),
                            Expanded(
                              child: Text(documentSnapshot["customerID"]),
                            ),
                            Expanded(
                              child: Text(documentSnapshot["customerEmail"]),
                            ),
                          ],
                        );
                      });
                } else {
                  return const Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: CircularProgressIndicator(),
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
